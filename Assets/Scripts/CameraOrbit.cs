﻿
using UnityEngine;

public class CameraOrbit : MonoBehaviour {



    protected Transform _Xform_Camera;
    protected Transform _Xform_Parent;

    protected Vector3 _LocalRotation;
    protected float _CameraDistance = 10f;

    public float mouseSensitivity = 4f;
   

    public float minDistanceToStartRotation = 2f;

    public float orbitDampening = 10f;

    


    public bool cameraDisabled = true;

    public float touchTime;

	// Use this for initialization
	void Start () {
        this._Xform_Camera = this.transform;
        this._Xform_Parent = this.transform.parent;
        touchTime = 0;
		
	}
	
	// Update is called once per frame
	void LateUpdate () {

        if(Input.GetMouseButton(0) && !Utils.rotationLock)
        {
            touchTime += Time.deltaTime;
            Debug.Log(touchTime);
            Utils.wasDragging = false;
            if(touchTime >= 0.2f)
            {
                Utils.wasDragging = true;
                cameraDisabled = false;
                if (!cameraDisabled)
                {
                    if (Input.GetAxis("Mouse X") != 0f)
                    {
                        //Getting the pointer's X axis and setting that as the local Y rotation for horizontal scrolling

                        _LocalRotation.y += Input.GetAxis("Mouse X") * mouseSensitivity;


                        Debug.Log(Input.GetAxis("Mouse X"));
                        //_LocalRotation.y = Mathf.Clamp(_LocalRotation.y, 0f, 90f);
                    }
                }



                //Camera Transformations

                Quaternion qT = Quaternion.Euler(0, _LocalRotation.y, 0);

                _Xform_Parent.rotation = Quaternion.Lerp(_Xform_Parent.rotation, qT, Time.deltaTime * orbitDampening);

            }
        }
        if(Input.GetMouseButtonUp(0))
        {
            touchTime = 0;
            cameraDisabled = true;
        }
     

       
      
	}

   


}
