﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProjectileFire : MonoBehaviour
{

    public GameObject projectile;
    public GameObject colorIndicatorBall;
    public GameObject UIButtons;

    public float shootForce = 8000f;
    public float numberOfBalls = 15;
    private float ballCount;

    public TextMeshPro BallCountText;
    public TextMeshProUGUI infoText;

   
    private Color nextColor;
   


    // Use this for initialization
    void Start()
    {
        Utils.score = 0;
        if(UIButtons!=null)
        {
            UIButtons.SetActive(false);
        }
        nextColor = NextBallColor();
        Utils.contactBlock = new HashSet<GameObject>();
        ballCount = numberOfBalls;
        BallCountText.text = ballCount.ToString();
        infoText.text = "";
        Utils.discoMode = false; 
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonUp(0) && !Utils.wasDragging && !Utils.rotationLock)
        {
            if (ballCount > 0)
            {
                ballCount -= 1;


                Vector3 hitPoint;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    hitPoint = hit.point;

                    //getting the cylinder we are about to shoot at
                    hitPoint = hit.transform.gameObject.transform.position;

                    //initialize a new contact block, which will then be computed by the cylinder we are about to hit
                    Utils.contactBlock = new HashSet<GameObject>();

                   



                    GameObject ball = Instantiate(projectile, Vector3.zero, Quaternion.identity);
                    ball.transform.SetParent(Camera.main.transform);
                    ball.transform.localPosition = new Vector3(0, -3, 15f);
                    ball.GetComponent<Renderer>().material.color = nextColor;

                    Vector3 shoot = (hitPoint - ball.transform.position).normalized;

                    Rigidbody rb = ball.GetComponent<Rigidbody>();
                    rb.AddForce(shoot * shootForce);
                    nextColor = NextBallColor();

                    
                        
                       
                    if (ballCount == 1)
                        infoText.text = "One Ball Left!";

                    if (ballCount == 0)
                    {
                        BallCountText.text = ballCount.ToString();

                        if (Utils.score >= Utils.scoreToWin)
                        {
                            infoText.text = "You Win! Your Score: " + Utils.score;
                        }
                        else
                        {
                            infoText.text = "Out Of Balls! Score: " + Utils.score;
                        }
                        if (UIButtons != null)
                        {
                            UIButtons.SetActive(true);
                        }
                    }




                }
                    
            }
            
          
            BallCountText.text = ballCount.ToString();
        }
    }

    //Randomly assign a ball color
    private Color NextBallColor()
    {
        Color color =  Utils.colors[Random.Range(0, Utils.colors.Length - 1)];
        if(colorIndicatorBall!=null)
        {
            colorIndicatorBall.GetComponent<Renderer>().material.color = color;
        }

        return color;
    }

    //A switch function to turn on and off the custom feature "Disco Mode". This is the ONLY place in the game where this bool is modified
    public void DiscoModeSwitch()
    {
        if (Utils.discoMode)
            Utils.discoMode = false;
        else
            Utils.discoMode = true;
    }
}
