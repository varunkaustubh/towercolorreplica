﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Navigation : MonoBehaviour {

	

    public void Restart()
    {
        SceneManager.LoadScene("GameScene");

    }

    public void CloseApp()
    {
        Application.Quit();
    }
}
