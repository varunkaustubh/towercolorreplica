﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CameraMovementScript : MonoBehaviour {

    public float speed;

    public bool projectileHit;
    public bool gameStarted;


    //number of cylinders destroyed before the camera moves down
    public int destroyLimitBeforeCameraMovesDown;
    public float yMovement;

    //UI Stuff
    public GameObject UIObjects;
    public TextMeshPro startText;

    private void Awake()
    {
        gameObject.GetComponent<Animator>().enabled = false;
    }

    private void Start()
    {
        //Initialize default start values

        startText.gameObject.SetActive(true);
        UIObjects.SetActive(false);
        Utils.destroyedCylinderCount = 0;
        gameStarted = false;
      
       
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0) && !gameStarted)
        {

            //First "Tap to start" Animation moving the camera transform
            gameStarted = true;
            Transform Parent = this.transform.parent;
            StartCoroutine(GameStartCameraMovement(Parent, new Vector3(0.2f, 35f, -33f), 2f));
        }

        //Check if the number of destroyed cylinders is equal to the limit set by us and move the camera down
        if (Utils.destroyedCylinderCount >= destroyLimitBeforeCameraMovesDown)
        {
            Debug.Log("Start Moving camera down!");
            Vector3 endpos = new Vector3(transform.localPosition.x, transform.localPosition.y - yMovement, transform.localPosition.z);
            StartCoroutine(MoveToPosition(transform, endpos, 1));
            Utils.destroyedCylinderCount = 0;
        }
    }

    

    //Using a simple jerk animation, Enabling and disabling the animator component for a fixed time value to "jerk" the camera
    //On impact with a matching cylinder
    public IEnumerator EnableAnimatorForSeconds(float time)
    {
        gameObject.GetComponent<Animator>().enabled = true;

        yield return new WaitForSeconds(time);

        gameObject.GetComponent<Animator>().enabled = false;
    }

    public void PlayJerkAnimation(float time)
    {
        StartCoroutine(EnableAnimatorForSeconds(time));
    }

  
    //Transformation function to move the camera from point A to point B. Using this to move the camera up and down

    public IEnumerator MoveToPosition(Transform transform, Vector3 position, float timeToMove)
    {
        if (transform.localPosition.y - yMovement > 15f)
        {
            var currentPos = transform.localPosition;
            var t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                transform.localPosition = Vector3.Lerp(currentPos, position, t);
                yield return null;
            }
        }
    }

    //A custom function to move the camera on game start (triggered on "Tap to start")
    public IEnumerator GameStartCameraMovement(Transform tr, Vector3 position, float timeToMove)
    {
        startText.gameObject.SetActive(false);

        var currentPos = transform.localPosition;
        var rotation = tr.rotation; 
            var t = 0f;
        Utils.rotationLock = true;
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                transform.localPosition = Vector3.Lerp(currentPos, position, t);
                tr.rotation = Quaternion.Euler(Vector3.Lerp(rotation.eulerAngles, new Vector3(rotation.eulerAngles.x, rotation.eulerAngles.y - 180f, rotation.eulerAngles.z), t));
                
                yield return null;
            }

        UIObjects.SetActive(true);
       
        Utils.rotationLock = false;
        
    }


}
