﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{

    // Contact Block is basically a list of cylinders of the same type that are directly or indirectly arranged together
    public static HashSet<GameObject> contactBlock;

    //this determines how many cylinders were destroyed, so the camera can then move down to show the next block of cylinders
    public static int destroyedCylinderCount;

    // bool check to prevent projectile fire while dragging across the screen to rotate the camera
    public static bool wasDragging;

    //bool to lock player camera rotation if needed (eg: to prevent rotation by player when game start animation is playing)
    public static bool rotationLock;

    //bool to trigger the custom feature "Disco Mode"
    public static bool discoMode;

    //Array of colors that are universally used in the game for assignment on cylinders and projectiles
    public static Color[] colors = { Color.blue, Color.green, Color.yellow, Color.red, Color.magenta };

    //Minimum score to win the game (1 cylinder destroyed = 1 point)
    public static int scoreToWin = 80;

    //Game Score
    public static int score;



}

