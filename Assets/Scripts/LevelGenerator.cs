﻿
//This is the main level generation script
//It takes a prefab of the cylinder and generates the main tower of the game and sets it up on game start
using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour
{

    //Basic level parameters

    public int numSteps = 10;
    public int numObjects = 15;
    public GameObject prefab;
    public float radius = 5.0f;

    //finer level parameters

    public Vector3 levelStartPosition;
    public float stepY = 1;

    


    void Start()
    {
        //"Steps" are basically the number of total steps in the tower
        for (int i = 0; i < numSteps; i++)
        {
            GameObject step = new GameObject();
            step.name = i.ToString();
            step.transform.position = new Vector3(levelStartPosition.x, levelStartPosition.y + stepY * i, levelStartPosition.z);
           
            Vector3 center = step.transform.position;

            //for each step, arrange the cylinders in a circular fashion, 1 unit above the height of the previous step
        for (int j = 0; j < numObjects; j++)
            {
                int ang = j * 30;
                Vector3 pos = RandomCircle(center, radius, ang);
                Quaternion rot = prefab.transform.rotation;

                GameObject cylinder = Instantiate(prefab, pos, rot);
                cylinder.GetComponent<Renderer>().material.color = Utils.colors[UnityEngine.Random.Range(0, Utils.colors.Length - 1)];

                cylinder.name = i.ToString() + "_" + j.ToString();
                cylinder.transform.SetParent(step.transform);
               
             

            }

            //Rotate every 2nd step by the specified amount to create an "Interlocking effect"
            if (i % 2 == 0)
            {
                step.transform.rotation = Quaternion.Euler(0, 10f, 0);

            }
            else
                step.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    //Not really random anymore! This is used to return points on the circle at regular intervals, where we will place our cylinders
    Vector3 RandomCircle(Vector3 center, float radius,int a)
    {
        float ang = a;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y;
        pos.z = center.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad); 
        return pos;
    }
}