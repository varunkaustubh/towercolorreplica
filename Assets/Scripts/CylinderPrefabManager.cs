﻿//This is a script that is attached to each cylinder in the tower.
//It provides functions for handling various game modes, collisions between the projectile and among other cylinders,
//And the main logic behind recursively building a list of attached cylinders and destroying them on projectile collision



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylinderPrefabManager : MonoBehaviour {

    public List<GameObject> touchingObjects;

    public float time;

    public GameObject particleSystem;
  

    private void Awake()
    {

        
    }

    // Use this for initialization
    void Start () {

        touchingObjects = new List<GameObject>();

       
        



       

}
	
	// Update is called once per frame
	void Update () {
       
        //Change this cylinder's color at regular intervals if disco mode is enabled
        if(Utils.discoMode)
        {
            time += Time.deltaTime;
            if(time>=1f)
            {
                gameObject.GetComponent<Renderer>().material.color = Utils.colors[UnityEngine.Random.Range(0, Utils.colors.Length - 1)];
                
                time = 0;
            }
        }
       
		
	}

    private void OnCollisionEnter(Collision collision)
    {
       

        //Add the neighbouring cylinders to this cylinder's list of touching objects, if the collision is not from the projectile.
        //If it is from the projectile, compare the material color from the list of touching objects on the cylinder
        //If the color matches with this cylinder, destroy it as well
        if (collision.gameObject.tag != "projectile" && collision.gameObject.tag!="base")
        {
            touchingObjects.Add(collision.gameObject);

        }
        else
        {
            //This condition is to check if we actually got hit by a projectile of the same color, and perform further actions
            if (collision.gameObject.GetComponent<Renderer>().material.color == gameObject.GetComponent<Renderer>().material.color)
            {
                Camera.main.SendMessage("PlayJerkAnimation", 0.3f);
                Destroy(collision.gameObject);

                //Recursive method to compute the number of cylinders of same color that are in contact
                BuildContactBlock();

                //Utils.ContactBlock will be populated with a list of all the cylinders from all the valid adjacent cylinders.
                //it is a global list to which the recursive function BuildContactBlock() adds values from all connected cylinders
                if (Utils.contactBlock!= null && Utils.contactBlock.Count > 0)
                {
                    foreach (GameObject touchingObject in Utils.contactBlock)
                    {

                        //Do stuff and destroy the contact block
                        if (touchingObject != null)
                        {
                            //Utils.contactBlock.Remove(touchingObject);

                            GameObject ps = Instantiate(particleSystem, touchingObject.transform.position, Quaternion.identity);

                           ParticleSystem.MainModule main = ps.GetComponent<ParticleSystem>().main;
                           main.startColor = touchingObject.GetComponent<Renderer>().material.color;

                            Destroy(touchingObject);
                           
                            Utils.destroyedCylinderCount += 1;
                            Utils.score += 1;
                            
                        }
                    }


                  
                }

                //Destroy this cylinder (after the contactblock has been destroyed)
                Debug.Log(Utils.contactBlock.Count + " Objects destroyed!");
                Utils.contactBlock.Clear();

                Utils.destroyedCylinderCount += 1;
                Utils.score += 1;
                Handheld.Vibrate();
                Destroy(gameObject);
            }

          
           
            
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "projectile")
        {
            StartCoroutine(DestroyObjectAfterSeconds(collision.gameObject, 3f));
            
        }

        if (touchingObjects.Contains(collision.gameObject))
        {
            touchingObjects.Remove(collision.gameObject);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if(!touchingObjects.Contains(collision.gameObject) && collision.gameObject.tag!="projectile" && collision.gameObject.tag!="base")
        {
            touchingObjects.Add(collision.gameObject);
        }

    }


    private IEnumerator DestroyObjectAfterSeconds(GameObject gO, float seconds)
    {
        yield return new WaitForSeconds(1f);
        if(gO!=null)
        {
            Destroy(gO);
        }
    }
   


    // This method is used to find a block of similar coloured cylinders that are attached to this cylinder, 
    //and among the other cylinders in contact with this one. it runs recursively to compute the region of similar blocks
    public void BuildContactBlock()
    {
        
        if(touchingObjects!=null && touchingObjects.Count > 0)
        {
           foreach(GameObject go in touchingObjects)
           {
                if (go != null && go.GetComponent<Renderer>().material.color == gameObject.GetComponent<Renderer>().material.color)
                {
                    if (Utils.contactBlock.Contains(go))
                        return;

                    else
                    {
                        Utils.contactBlock.Add(go);
                        go.GetComponent<CylinderPrefabManager>().BuildContactBlock();

                    }
                        
                    Debug.Log(go.name);
                    
                }
           }
        }
    }

    


}
